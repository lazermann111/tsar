package com.internetwarz.basketballrush.model;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class PlayerTurn implements Serializable {




    /*CONSTANTS*/
    public final static int MATCH_TURN_STATUS_INVITED = 0;
    public final static int MATCH_TURN_STATUS_MY_TURN = 1;
    public final static int  MATCH_TURN_STATUS_THEIR_TURN = 2;
    public final static int MATCH_TURN_STATUS_COMPLETE = 3;

    // round workflow :
    // MATCH_ROUND_STATUS_PLAYER1_ZAGADYVAET  -> MATCH_ROUND_STATUS_PLAYER2_OTGADYVAET -> MATCH_ROUND_STATUS_PLAYER2_OTGADAL ->
   //  -> MATCH_ROUND_STATUS_PLAYER2_ZAGADYVAET ->  MATCH_ROUND_STATUS_PLAYER1_OTGADYVAET ->  MATCH_ROUND_STATUS_PLAYER1_OTGADAL -> new round

    public final static int MATCH_ROUND_STATUS_PLAYER1_ZAGADYVAET = 5;
    public final static int MATCH_ROUND_STATUS_PLAYER2_ZAGADYVAET = 6;

    public final static int MATCH_ROUND_STATUS_PLAYER1_OTGADYVAET = 7;
    public final static int MATCH_ROUND_STATUS_PLAYER2_OTGADYVAET = 8;

    public final static int MATCH_ROUND_STATUS_PLAYER1_OTGADAL = 9;
    public final static int MATCH_ROUND_STATUS_PLAYER2_OTGADAL = 10;

    /*PERSISTED FIELDS*/
    public int selectedNumber;
    public int zagadannoeNumber;
    public int turnCounter;
    public String player1Id;
    public int player1Score;
    public int player2Score;


    /*NON-PERSISTED FIELDS*/  //need to fill manually on client
    public int turnStatus;
    public int roundStatus;

    public byte[] persist() {
        try {
            return convertToBytes(this);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static PlayerTurn unpersist(byte[] data) throws IOException, ClassNotFoundException {


            return (PlayerTurn) convertFromBytes(data);

    }


    private byte[] convertToBytes(Object object) throws IOException {

        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutput out = new ObjectOutputStream(bos) ;
        out.writeObject(object);

       return bos.toByteArray();



    }



    private static Object convertFromBytes(byte[] bytes) throws IOException, ClassNotFoundException {
        ByteArrayInputStream bis = new ByteArrayInputStream(bytes);
        ObjectInput in = new ObjectInputStream(bis);

        return in.readObject();

    }

    @Override
    public String toString() {
        return "PlayerTurn{" +
                "selectedNumber=" + selectedNumber +
                "zagadannoeNumber=" + zagadannoeNumber +
                ", turnCounter=" + turnCounter +
                ", player1Id='" + player1Id + '\'' +

                ", player1Score=" + player1Score +
                ", player2Score=" + player2Score +
                '}';
    }
}
